# WebGL Supernova - Three.js

GPGPU Particle Supernova using Curl over Value Noise. Achieved via Three.js in WebGL.

Highlights:
- GPGPU via GPUComputationRenderer.js
- Procedurally generated
- 4D Curl Noise over Value Noise
- Fully GPU Optimized
- Fully Customizable via URL Params

Demo - [g7495x.gitlab.io/webgl-supernova-three.js](https://g7495x.gitlab.io/webgl-supernova-three.js/)

Demo with Controls - [g7495x.gitlab.io/webgl-supernova-three.js/?showControls=true](https://g7495x.gitlab.io/webgl-supernova-three.js/?showControls=true)

![](Screenshot-01.png)
![](Screenshot-02.png)
![](Screenshot-03.png)
![](Screenshot-04.png)
![](Screenshot-05.png)
![](Screenshot-06.png)
